#### 介绍

基于docker的php7.4开发环境，一键部署PHP、MySQL、Nginx、Redis、Memcached、Mongodb等
包含核心扩展：gd intl bcmath sockets pcntl mysqli pdo_mysql opcache zip
PECL扩展：mcrypt redis memcache mongodb xlswriter imagick amqp swoole protobuf
各种PHP框架拿来即用：Yii Thinkphp Laravel Swoole Hyperf...

#### 第1步：创建docker网络

~~~shell
docker network create ruiorznet
~~~

#### 第2步：设置环境变量

~~~ shell
DNMP_DIR="/d/dnmp"            # 特别重要，一定要填写docker-compose.yml文件所在的目录，且windows下别写D:\dnmp这种

NGINX_PORT=8888               # nginx端口

PHP_FPM_PORT=9001             # fpm端口

MYSQL_PORT=3307               # MySQL端口
MYSQL_ROOT_PASSWORD=123456    # MySQL的root密码

REDIS_PORT=6380               # redis端口
REDIS_PASSWORD=123456         # redis连接密码

MONGO_PORT=27018              # mongodb端口

MEMCACHED_PORT=11212          # memcached端口
~~~



#### 第3步：运行编排

~~~shell
docker-compose up -d
~~~



#### 第4步：大功告成

~~~shell
# 访问phpinfo页面
http://localhost:8888

# 各个容器的配置文件都在各自的目录
# 下面是目录结构：
.
|____.env
|____docker-compose.yml
|____mongo
| |____db
| |____log
| | |____mongod.log
| |____mongod.conf
|____mysql
| |____data
| |____logs
| | |____mysqld.log
| |____my.cnf
|____nginx
| |____conf
| | |____localhost.conf
| |____logs
| | |____access.log
| | |____error.log
| |____nginx.conf
|____php
| |____php.ini
| |____www.conf
|____README.md
|____redis
| |____data
| |____redis.conf
|____wwwroot
| |____l.php
~~~

常用命令：

~~~shell
# PHP
docker exec -it local-php php -v # 查看PHP版本
docker exec -it local-php php -m # 查看PHP已安装扩展
docker exec -it local-php composer -V # 查看composer版本

# MySQL
docker exec -it local-mysql mysql --version # 查看MySQL版本

# nginx
docker exec -it local-nginx nginx -s reload|stop|start # 重启、停止、启动
~~~

#### 配置文件



#### 其他

~~~shell
# 如果发现mysql有警告:
mysql: [Warning] World-writable config file '/etc/my.cnf' is ignored.

docker exec -it local-mysql /bin/bash # 进入mysql容器
# 在容器内交互命令行执行：
chmod 644 /etc/my.cnf

# 退出容器，重启
docker restart local-mysql
~~~

